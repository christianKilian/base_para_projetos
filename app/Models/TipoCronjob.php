<?php

namespace App\Models;

use App\Models\BaseModel;

class TipoCronjob extends BaseModel
{
    protected $table = "tipo_cronjob";
    protected $primaryKey = "tcron_id";

    //cont CAMPO_CONSTANTE_TABLE = ID
    const TRADUZIR = 1;
}