<?php

namespace App\Models;

use App\Models\BaseModel;

class Perfil extends BaseModel
{
    protected $table = "perfil";
    protected $primaryKey = "per_id";

    const ADMINISTRADOR           = 1;
    const COMUM                   = 2;

}
