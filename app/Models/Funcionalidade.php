<?php

namespace App\Models;

use App\Models\BaseModel;

class Funcionalidade extends BaseModel
{
    protected $table = "funcionalidade";
    protected $primaryKey = "fun_id";
}
