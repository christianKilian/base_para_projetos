<?php

namespace App\Models;

class PerfilFuncionalidade extends BaseModel
{
    protected $table = "perfil_funcionalidade";
    protected $primaryKey = "pfu_id";
}
