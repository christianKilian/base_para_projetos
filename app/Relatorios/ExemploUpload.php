<?php

namespace App\Relatorios;

use App\API\DefaultExcelLoaderApi;
use App\Exceptions\NegocioException;
use App\Services\FabricaService;
use App\Models\Perfil;
use PhpOffice\PhpSpreadsheet\Shared\Date as DatePHPExcel;

class UsuarioUpload extends DefaultExcelLoaderApi
{
	public $dados;
	private $fabricas;
	private $sheet;
	private $sheet_coluns;
	private $sheet_rules;
	private $sheet_names;


	public function __construct($file,$constants)
	{
		parent::__construct($file);
		if($this->loadStatus)
		{
			$this->dados    = [];
			$this->fabricas = $constants;
			try
			{
				$this->setParams()->loadDataFromFile();
			}
			catch (NegocioException $e)
			{
				$this->loadStatus = false;
				$this->mensagem = $e->getMessage();
			}
		}
		return $this;
	}
	private function setParams()
	{
		$this->sheet = $this->excel->getSheet(0);
		$this->sheet_coluns = [
			'usu_id_ambev' => 1,
			'usu_nome'     => 2,
			'fab_id'       => 3,
			'usu_idioma'   => 4
		];
		$this->sheet_rules = [
			'usu_id_ambev' => 'required',
			'usu_nome'     => 'required|max:156',
		];
		$this->sheet_names = [
			'usu_id_ambev' => trans('app.excel-upload.usuario.coluna.usu_id_ambev'),
			'usu_nome'     => trans('app.excel-upload.usuario.coluna.usu_nome'),
		];
		return $this;
	}
	/**
	 * [Método que carrega os dados da tabela do excel]
	 * @return [array] [conteudo da tabela]
	 */
	private function loadDataFromFile()
	{
		for($row = 2; $row <= $this->sheet->getHighestRow(); $row++)
		{
			$t = [];
			foreach ($this->sheet_coluns as $column_name => $column_index )
			{
				$t[$column_name] = trim($this->sheet->getCellByColumnAndRow($column_index,$row)->getValue());
			}
			if($this->checkInvalidRow($t))continue;
			$this->validaRow($t, $row);
			$this->dados[] = $t;
		}
		return $this;
	}
	/**
	 * [METODO QUE VERIFICA SE A LINHA TEM AS CELULAS NECESSARIAS PARA QUE SEJA GRAVADAS AS INFORMAÇÕES NO BANCO DE DADOS]
	 * @param  [array] $row [linha da tabela do excel]
	 * @return [boolean]
	 */
	private function checkInvalidRow($row)
	{
		return ( 
			empty($row['usu_id_ambev']) && 
			empty($row['usu_nome']) && 
			empty($row['fab_id'])  
		);
	}
	/**
	 * [METODO QUE FAZ A VALIDAÇÃO DOS DADOS E APLICA REGRAS ESPECIFICAS]
	 * @param  [array] &$data [linha da tabela]
	 * @param  [int] $row     [numero da linha da tabela]
	 * @return [void]
	 */
	private function validaRow(&$data, $row)
	{
		$_tmp = [
			'usu_id_ambev' => trans('validation.excel_upload.row',["attribute"=>$this->sheet_names['usu_id_ambev'],"linha"=>"{$row}"]),
			'usu_nome'     => trans('validation.excel_upload.row',["attribute"=>$this->sheet_names['usu_nome']    ,"linha"=>"{$row}"]),
		];
		//
		$this->_validate($data,$this->sheet_rules,$_tmp);
		//AQUI VAI VALIDAR A FABRICA ID, SE TIVER VALOR E NÃO EXISTIR NO ARRAY DE FABRICAS TROLA ERRO, CASO PASSE, APLICA REGRA DE ATRIBUIÇÃO DE PERFIL
		if(!empty($data['fab_id']) && !array_key_exists($data['fab_id'], $this->fabricas))
		{
			throw new NegocioException(
				trans("validation.exists",
				[
					'attribute'=>
						trans("validation.excel_upload.row",["attribute"=>$data['fab_id'],"linha"=>$row])
				]));
		}
		else
		{
			$data['fab_id'] = !empty($data['fab_id']) 
				? $this->fabricas[$data['fab_id']]
				: null
			;
		}
		$data["per_id"] = !empty($data['fab_id'])
			? Perfil::FABRICA
			: Perfil::ADMINISTRADOR;
		$data['usu_idioma'] = $data['usu_idioma'] ?? env('APP_LOCALE','en');
	}
}