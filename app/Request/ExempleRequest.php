<?php

namespace App\Request;

use Illuminate\Foundation\Http\FormRequest;

class ExempleRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //irá conter todos as regras de valição do request
            /*
                'name' => 'required',
            */
        ];
    }

    /* Mensagens personalizadas */
    // public function messages()
    // {
    //     return [

    //     ];
    // }
}
