<?php

namespace App\API;

use PhpOffice\PhpSpreadsheet\IOFactory;
use App\Libraries\HelperLib;
use App\Exceptions\NegocioException;
use App\Traits\HelperTrait;

class DefaultExcelLoaderApi
{
	use HelperTrait;
	public $loadStatus;
	public $mensagem;
	private $nomeArquivo;
	private $caminhoCompleto;
	private $path;
	private $file;
	protected $excel;

	public function __construct($file)
	{
		$this->path = config('mofab.files-path.metas');
		$this->nomeArquivo = $this->_hash();
		$this->file = $file;
		$this->loadStatus = true;

		try
		{
			$this->validarArquivo()->loadFile();
		}
		catch (NegocioException $e)
		{
			$this->loadStatus = false;
			$this->mensagem = 'Dados inválidos';
		}
		return $this;
	}

	private function validarArquivo()
	{
		$rules =[
			'file' => 'required',
            'name' => 'required',
            'size' => 'required',
            'type' => 'required'
		];
		
		$this->_validate($this->file, $rules);
		
		return $this;
	}

	private function loadFile()
	{
		$exp = explode('.', $this->file['name']);
		$ext = array_pop($exp);
		$this->caminhoCompleto = $this->path.$this->nomeArquivo.'.'.$ext;
		$fileExp = explode(';base64,', $this->file['file']);
		file_put_contents($this->caminhoCompleto, base64_decode($fileExp[1]));
		$this->excel = IOFactory::load($this->caminhoCompleto);
		unlink($this->caminhoCompleto);
		return $this;
	}
}