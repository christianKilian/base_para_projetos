<?php

namespace App\API;

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\Color as PHPExcelColor;
use PhpOffice\PhpSpreadsheet\Style\Fill as PHPExcelFill;
use App\Traits\HelperTrait;

class RelatorioAPI
{
	use HelperTrait;
	public $excel;
	public $nomeArquivo;

	public function __construct()
	{
		$this->excel = new Spreadsheet();
	}
	public function montaArquivo($dados)
	{
		$this->excel->setActiveSheetIndex(0);
		$temp = array_keys(get_object_vars($dados[0]));
		$this->montaCabecalho($temp);
		$this->populaValores($dados);
		$this->setHeaders($this->nomeArquivo);
	}
	public function setaAtributos($critator = null, $lastModifiedBy = null, $title = null, $subject = null, $description = null)
	{
		$this->excel->getProperties()
			->setCreator("{$critator}")
			->setLastModifiedBy("{$lastModifiedBy}")
			->setTitle("{$title}")
			->setSubject("{$subject}")
			->setDescription("{$description}");
	}
	public function montaCabecalho($dados, $argb_color = 'FFFF0000')
	{
		$columns = 1;
		foreach ($dados as $k => $v)
		{
			$this->excel->getActiveSheet()->getColumnDimension($this->stringColumn($columns-1))->setAutoSize(true);
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow($columns++, 1, $v);
		}
		$columnas = 'A1:'.$this->stringColumn($columns-2).'1';
		$this->excel->getActiveSheet()->getStyle($columnas)->getFill()->setFillType(PHPExcelFill::FILL_SOLID)->getStartColor()->setARGB($argb_color);
		$this->excel->getActiveSheet()->getStyle($columnas)->getFont()->getColor()->setARGB(PHPExcelColor::COLOR_WHITE);
		$this->excel->getActiveSheet()->getStyle($columnas)->getFont()->setBold(true);
	}
	public function populaValores($dados)
	{
		$row = 2;
		$column = 1;
		foreach ($dados as $key => $value)
		{
			$column = 1;
			foreach ($value as $k => $v)
			{
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow($column++, $row, $v);
			}
			$row++;
		}
	}
	public function setHeaders($nomeArquivo = 'relatorio.xls')
	{
		ob_start();
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$nomeArquivo.'"');
		header('Cache-Control: max-age=0');
		header('Cache-Control: max-age=1');
		$writer = IOFactory::createWriter($this->excel, 'Xls'); // USAR MODO 'Excel5' OU 'HTML'
		// $writer->setPreCalculateFormulas(true);
		ob_clean();
		return $writer->save('php://output');
	}
	public function stringColumn($index)
	{
		$pos = array(
			'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
			'AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ',
			'BA','BB','BC','BD','BE','BF','BG','BH','BI','BJ','BK','BL','BM','BN','BO','BP','BQ','BR','BS','BT','BU','BV','BW','BX','BY','BZ',
			'CA','CB','CC','CD','CE','CF','CG','CH','CI','CJ','CK','CL','CM','CN','CO','CP','CQ','CR','CS','CT','CU','CV','CW','CX','CY','CZ',
			'DA','DB','DC','DD','DE','DF','DG','DH','DI','DJ','DK','DL','DM','DN','DO','DP','DQ','DR','DS','DT','DU','DV','DW','DX','DY','DZ',
			'EA','EB','EC','ED','EE','EF','EG','EH','EI','EJ','EK','EL','EM','EN','EO','EP','EQ','ER','ES','ET','EU','EV','EW','EX','EY','EZ',
			'FA','FB','FC','FD','FE','FF','FG','FH','FI','FJ','FK','FL','FM','FN','FO','FP','FQ','FR','FS','FT','FU','FV','FW','FX','FY','FZ',
			'GA','GB','GC','GD','GE','GF','GG','GH','GI','GJ','GK','GL','GM','GN','GO','GP','GQ','GR','GS','GT','GU','GV','GW','GX','GY','GZ',
			'HA','HB','HC','HD','HE','HF','HG','HH','HI','HJ','HK','HL','HM','HN','HO','HP','HQ','HR','HS','HT','HU','HV','HW','HX','HY','HZ',
			'IA','IB','IC','ID','IE','IF','IG','IH','II','IJ','IK','IL','IM','IN','IO','IP','IQ','IR','IS','IT','IU','IV');
		return $pos[$index];
	}
	public function __toString()
	{
		return 'Relatorio gerado com sucesso!!!';
	}
}