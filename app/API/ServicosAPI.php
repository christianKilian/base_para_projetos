<?php

namespace App\API;

use App\Models\ApiServico;
use Carbon\Carbon;
use App\Traits\EncryptTrait;
use Log;
use Exception;

class ServicosAPI
{
	use EncryptTrait;
	/**
	 * Verifica se a request possui o hash do cliente e o segredo
	 * criptografado pela chave publica fornecida.
	 *
	 * @param string $mofab-client-id // hash fornecido para o cliente previamente
	 * @param string (base64_encoded) $mofab-data // data e hora (GMT 0) criptografado pela chave publica
	 * @return boolean
	 *
	 */
	public function verificaHeader($request)
	{
		// Recupera a chave privada e o hash
		$chave  = $this->carregaChave($request->header()['mofab-service-id']);
		$hash   = $request->header()['mofab-data'][0];
		$hora   = null;
		$status = false;
		try 
		{
			if(!empty($chave))
			{
				$hora = $this->_private_decrypt($chave, $hash);
				if((bool)strtotime($hora)) $status = $this->verificaData($hora);
			}
		} 
		catch (Exception $e) 
		{
			debug([
				'line' => $e->getLine(),
				'code' => $e->getCode(),
				'erro' => $e->getMessage(),
				'trace' => $e->getTraceAsString()
			]);
		}

		return $status;
	}
	/**
	 * A partir da hash enviada pelo cliente, verifica na base se o cliente existe
	 * e carrega a chave publica do mesmo
	 *
	 * @param string $clientHash
	 * @return string ($cli_private_key)
	 *
	 */
	private function carregaChave($clientHash)
	{
		// verifica se o client ID fornecido existe e retorna a chave privada do mesmo
		$cliente = ApiServico::where('asv_cod_cliente',$clientHash)
			->where('asv_ativo',ApiServico::ATIVO)
			->first();
		return $cliente->asv_chave_priv ?? false;
	}
	/**
	 * Verifica se a data e hora enviada estão dentro do padrão configurado
	 * (config -> mofab -> servicos -> diff_valid_token )
	 *
	 * @param string $data
	 * @return boolean
	 *
	 */
	private function verificaData($token)
	{
		// define o padrão de timezone para GMT-0 para tratamento das datas
		date_default_timezone_set('UTC');
		// formata a data enviada para o padrão Ano-mes-dia Hora:minutos:segundos
		$data = Carbon::parse($token)->timezone(0);
		// verifica se a diferença em segundos é maior que o padrão determinado
		$status = ! (Carbon::now(0)->diffInSeconds($data) > config('mofab.servicos.diff_valid_token'));
		
		date_default_timezone_set(config('app.timezone'));
		
		return $status;
	}
	/**
	 * Retorna a resposta padrão quando o token é inválido
	 *
	 * @return array
	 *
	 */
	public function InvalidResponse()
	{
		return [
			'status' => 0,
			'response' => 'Unauthorized request due to invalid token'
		];
	}
}