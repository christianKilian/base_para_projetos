<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RedirectController extends Controller
{
	public function acessonegado()
	{
		return view('telasDeErros.acessonegado');
	}
	public function pageNotFound()
	{
		return view('telasDeErros._error404');
	}
	public function generalError()
	{
		return view('telasDeErros._error405');
	}
}
