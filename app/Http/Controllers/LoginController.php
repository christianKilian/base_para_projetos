<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function index()
    {
        return view('painel.login.login');
    }
    public function login(Request $request)
    {
        // return $this->_callService('LoginService','login',$request->all());
        $retornoNegocio = $this->_callService('LoginService','login',$request->all());
        if(!empty($retornoNegocio) && $retornoNegocio['status'])
        {
            $usuario = $this->_convertObject($retornoNegocio['response']);
            session()->put('user', $usuario);
            return $retornoNegocio;
        }
        return redirect()->route('login.index');
    }

    public function logout()
    {
        Auth::guard('web')->logout(false);
        session()->flush();
        return redirect()->route('site.home');
    }
}
