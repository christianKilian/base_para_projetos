<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class PainelController extends Controller
{
    public function index()
    {
        if(!Session::get('user') && !Auth::user()){
            return redirect()->to('login.index');
        }
        return view('painel.home');
    }

    public function mailTest(Request $request)
    {
        return $this->_callService('PainelService', 'sendMail', $request->all());
    }
}
