<?php

namespace App\Http\Middleware;

use Closure;
use App\Libraries\HelperLib;

class VerifyAccessLayer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(session()->has('user'))
        {
            return HelperLib::verificaAcesso('/'.$request->path())
                ? $next($request)
                : redirect()->guest(route('errors.acessonegado'))
            ;
        }
        else
        {
            return $next($request);
        }
    }
}
