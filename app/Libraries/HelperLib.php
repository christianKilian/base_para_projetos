<?php

namespace App\Libraries;

use App\Exceptions\NegocioException;
use Illuminate\Support\Facades\Auth;
use Validator;

class HelperLib
{
    public static function createUniqueFileName()
	{
		$a = microtime(true);
		$b = date("Y-m-d H:i:s");
		$c = rand(000, 999);
		$d = rand(000, 999);
		return hash('sha256', (sha1($a) . md5($b) . sha1($c) . md5($d)));
	}
	public static function verificaAcesso($url)
	{
		$temAcesso = true;
		if 	( !strpos($url, 'rest') && !strpos($url, 'login') && !strpos($url, 'logout') && ($url != config('app.url')) )
		{
			$temAcesso = false;
			if(!in_array(str_replace(config('app.url'), '', $url) , session()->get('user')->ROTAS_SISTEMA))
			{
				 $temAcesso = true;
			}
			else
			{
				if(in_array(str_replace(config('app.url'), '', $url), session()->get('user')->ROTAS_ACESSO))
				{
					$temAcesso = true;
				}
			}
		}
		return $temAcesso;
	}
}