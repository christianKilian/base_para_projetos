<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use TheSeer\Tokenizer\Exception;

class AppStartCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:ativar';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '
        Esse comando serve para iniciar um projeto novo do zero, criando a estrutura basica da base de dados
    ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        printf("\nInicio da criação da estrutura da base de dados");
        try{
            DB::transaction(function () {

                Schema::create('funcionalidade', function (Blueprint $table) {
                    $table->increments('fun_id');
                    $table->text('fun_url')->nullabel();
                    $table->text('fun_descricao')->nullabel();
                });

                Schema::create('perfil', function (Blueprint $table) {
                    $table->increments('per_id');
                    $table->text('per_descricao');
                });

                Schema::create('perfil_funcionalidade', function (Blueprint $table) {
                    $table->increments('pfu_id');
                    $table->integer('per_id');
                    $table->foreign('per_id')->references('per_id')->on('perfil');
                    $table->integer('fun_id');
                    $table->foreign('fun_id')->references('fun_id')->on('funcionalidade');
                });

                Schema::create('usuario_sistema', function (Blueprint $table) {
                    $table->increments('usu_id');
                    $table->string('usu_nome', 156);
                    $table->string('usu_senha', 45);
                    $table->string('usu_username', 80);
                    $table->string('usu_email', 60)->nullabel();
                    $table->unsignedInteger('per_id');
                    $table->foreign('per_id')->references('per_id')->on('perfil');
                    $table->smallInteger('usu_ativo')->default(1);
                    $table->timestamp('usu_data_criacao')->useCurrent();
                    $table->timestamp('usu_ultimo_acesso')->nullable();
                });

                //table de log
                Schema::create('log_utilizacao', function (Blueprint $table) {
                    $table->increments('lga_id');
                    $table->integer('usu_id')->nullable();
                    $table->text('lga_dados')->nullable();
                    $table->timestamp('lga_data')->useCurrent();

                });

                //table tipo_cronjob
                Schema::create('tipo_cronjob', function (Blueprint $table) {
                    $table->increments('tcron_id');
                    $table->string('tcron_constante', 50);
                    $table->timestamp('created_at')->useCurrent();
                    $table->timestamp('updated_at')->nullable();
                    $table->timestamp('deleted_at')->nullable();
                });

                //table cronjob_log
                Schema::create('cronjob_log', function (Blueprint $table) {
                    $table->increments('cronl_id');
                    $table->unsignedInteger('tcron_id');
                    $table->foreign('tcron_id')->references('tcron_id')->on('tipo_cronjob');
                    $table->timestamp('cronl_data_inicio')->useCurrent();
                    $table->timestamp('cronl_data_fim')->nullable();
                    $table->smallInteger('cronl_status')->nullable();
                    $table->text('cronl_mensagem', 500)->nullable();
                });

                //table de tradução
                Schema::create('traducoes', function (Blueprint $table) {
                    $table->increments('trd_id');
                    $table->text('trd_chave')->unique();
                    $table->text('pt')->nullable();
                    $table->text('en')->nullable();
                    $table->text('es')->nullable();

                });

                printf("\nbase criada, iniciando a população das tabelas com algumas configurações padrão.");

                DB::table('perfil')->insert([
                    ['per_id' => 1,
                    'per_descricao' => 'Administrador',
                    ],
                    ['per_id' => 2,
                    'per_descricao' => 'Comum',
                    ],
                ]);

                DB::table('usuario_sistema')->insert([
                    'usu_nome' => 'Christian',
                    'usu_senha' => sha1('123456'),
                    'usu_username' => 'christian',
                    'usu_email' => 'chkilian89@gmail.com',
                    'per_id' => 1
                ]);

                DB::table('tipo_cronjob')->insert([
                    'tcron_constante' => 'TRADUZIR'
                ]);


                DB::table('traducoes')->insert([
                    [
                        'trd_chave' => "auth.failed",
                        'pt' => "Suas credenciais não conferem com seu registro.",
                        'en' => "These credentials do not match our records."
                    ], [
                        'trd_chave' => "auth.throttle",
                        'pt' => "Muitas tentativas de login. Por favor, tente novamente em :seconds segundos",
                        'en' => "Too many login attempts. Please try again in :seconds seconds."
                    ], [
                        'trd_chave' => "pagination.next",
                        'pt' => "Próximo &raquo;",
                        'en' => "Next &raquo;"
                    ], [
                        'trd_chave' => "pagination.previous",
                        'pt' => "&laquo; Anterior",
                        'en' => "&laquo; Previous"
                    ], [
                        'trd_chave' => "passwords.password",
                        'pt' => "Senha deve ter ao menos seis caracteres e ser igual a confirmação",
                        'en' => "Passwords must be at least six characters and match the confirmation."
                    ], [
                        'trd_chave' => "passwords.reset",
                        'pt' => "Sua senha foi alterada!",
                        'en' => "Your password has been reset!"
                    ], [
                        'trd_chave' => "passwords.sent",
                        'pt' => "Um e-mail com o link de alteração da senha foi enviado para você!",
                        'en' => "We have e-mailed your password reset link!"
                    ], [
                        'trd_chave' => "passwords.token",
                        'pt' => "Esse token de alteração de senha é inválido.",
                        'en' => "This password reset token is invalid."
                    ], [
                        'trd_chave' => "passwords.user",
                        'pt' => "Não foi encontrado um usuário com o e-mail informado.",
                        'en' => "We can't find a user with that e-mail address."
                    ], [
                        'trd_chave' => "validation.accepted",
                        'pt' => "O :attribute deve ser aceito.",
                        'en' => "The :attribute must be accepted."
                    ], [
                        'trd_chave' => "validation.active_url",
                        'pt' => "O :attribute não é uma URL válida.",
                        'en' => "The :attribute is not a valid URL."
                    ], [
                        'trd_chave' => "validation.after",
                        'pt' => "O :attribute deve ser uma data depois de :date.",
                        'en' => "The :attribute must be a date after :date."
                    ], [
                        'trd_chave' => "validation.after_or_equal",
                        'pt' => "O :attribute deve ser uma data depois ou igual á :date.",
                        'en' => "The :attribute must be a date after or equal to :date."
                    ], [
                        'trd_chave' => "validation.alpha",
                        'pt' => "O :attribute deve conter apenas letras.",
                        'en' => "The :attribute may only contain letters."
                    ], [
                        'trd_chave' => "validation.alpha_dash",
                        'pt' => "O :attribute deve conter apenas letras, números, e traços.",
                        'en' => "The :attribute may only contain letters, numbers, and dashes."
                    ], [
                        'trd_chave' => "validation.alpha_num",
                        'pt' => "O :attribute deve conter apenas letras e números.",
                        'en' => "The :attribute may only contain letters and numbers."
                    ], [
                        'trd_chave' => "validation.array",
                        'pt' => "O :attribute deve ser um array.",
                        'en' => "The :attribute must be an array."
                    ], [
                        'trd_chave' => "validation.attributes.fab_id",
                        'pt' => "Fábrica",
                        'en' => "Factory"
                    ], [
                        'trd_chave' => "validation.before",
                        'pt' => "O :attribute deve ser uma data antes :date.",
                        'en' => "The :attribute must be a date before :date."
                    ], [
                        'trd_chave' => "validation.before_or_equal",
                        'pt' => "O :attribute deve ser uma data antes ou igual á :date.",
                        'en' => "The :attribute must be a date before or equal to :date."
                    ], [
                        'trd_chave' => "validation.between.array",
                        'pt' => "O :attribute must have entre :min e :max itens.",
                        'en' => "The :attribute must have between :min and :max items."
                    ], [
                        'trd_chave' => "validation.between.file",
                        'pt' => "O :attribute deve ser entre :min e :max kilobytes.",
                        'en' => "The :attribute must be between :min and :max kilobytes."
                    ], [
                        'trd_chave' => "validation.between.numeric",
                        'pt' => "O :attribute deve ser entre :min e :max.",
                        'en' => "The :attribute must be between :min and :max."
                    ], [
                        'trd_chave' => "validation.between.string",
                        'pt' => "O :attribute deve ser entre :min e :max caracteres.",
                        'en' => "The :attribute must be between :min and :max characters."
                    ], [
                        'trd_chave' => "validation.boolean",
                        'pt' => "O campo :attribute deve ser true ou false.",
                        'en' => "The :attribute field must be true or false."
                    ], [
                        'trd_chave' => "validation.confirmed",
                        'pt' => "O :attribute confirmação não corresponde.",
                        'en' => "The :attribute confirmation does not match."
                    ], [
                        'trd_chave' => "validation.custom.attribute-name.rule-name",
                        'pt' => "custom-message",
                        'en' => "custom-message"
                    ], [
                        'trd_chave' => "validation.date",
                        'pt' => "O :attribute não é uma data válida.",
                        'en' => "The :attribute is not a valid date."
                    ], [
                        'trd_chave' => "validation.date_format",
                        'pt' => "O :attribute não corresponde ao formato :format.",
                        'en' => "The :attribute does not match the format :format."
                    ], [
                        'trd_chave' => "validation.different",
                        'pt' => "O :attribute e :other devem ser diferentes.",
                        'en' => "The :attribute and :other must be different."
                    ], [
                        'trd_chave' => "validation.digits",
                        'pt' => "O :attribute deve ser :digits digitos.",
                        'en' => "The :attribute must be :digits digits."
                    ], [
                        'trd_chave' => "validation.digits_between",
                        'pt' => "O :attribute deve ser entre :min e :max digitos.",
                        'en' => "The :attribute must be between :min and :max digits."
                    ], [
                        'trd_chave' => "validation.dimensions",
                        'pt' => "O :attribute tem dimensões de imagem inválidas.",
                        'en' => "The :attribute has invalid image dimensions."
                    ], [
                        'trd_chave' => "validation.distinct",
                        'pt' => "O campo :attribute tem valor duplicado.",
                        'en' => "The :attribute field has a duplicate value."
                    ], [
                        'trd_chave' => "validation.email",
                        'pt' => "O :attribute deve ser um endereço de e-mail válido.",
                        'en' => "The :attribute must be a valid email address."
                    ], [
                        'trd_chave' => "validation.excel_upload.row",
                        'pt' => ":attribute na linha :linha ",
                        'en' => ""
                    ], [
                        'trd_chave' => "validation.exists",
                        'pt' => "O :attribute selecionado é inválido.",
                        'en' => "The selected :attribute is invalid."
                    ], [
                        'trd_chave' => "validation.file",
                        'pt' => "O :attribute deve ser um arquivo.",
                        'en' => "The :attribute must be a file."
                    ], [
                        'trd_chave' => "validation.filled",
                        'pt' => "O campo :attribute deve ter valor.",
                        'en' => "The :attribute field must have a value."
                    ], [
                        'trd_chave' => "validation.image",
                        'pt' => "O :attribute deve ser uma image.",
                        'en' => "The :attribute must be an image."
                    ], [
                        'trd_chave' => "validation.in",
                        'pt' => "O :attribute selecionado é inválido.",
                        'en' => "The selected :attribute is invalid."
                    ], [
                        'trd_chave' => "validation.in_array",
                        'pt' => "O campo :attribute não existe em :other.",
                        'en' => "The :attribute field does not exist in :other."
                    ], [
                        'trd_chave' => "validation.integer",
                        'pt' => "O :attribute deve ser um inteiro.",
                        'en' => "The :attribute must be an integer."
                    ], [
                        'trd_chave' => "validation.ip",
                        'pt' => "O :attribute deve ser um endereço de IP válido .",
                        'en' => "The :attribute must be a valid IP address."
                    ], [
                        'trd_chave' => "validation.ipv4",
                        'pt' => "O :attribute deve ser um endereço de IPv4 válido .",
                        'en' => "The :attribute must be a valid IPv4 address."
                    ], [
                        'trd_chave' => "validation.ipv6",
                        'pt' => "O :attribute deve ser um endereço de IPv6 válido .",
                        'en' => "The :attribute must be a valid IPv6 address."
                    ], [
                        'trd_chave' => "validation.json",
                        'pt' => "O :attribute deve ser um texto JSON válido .",
                        'en' => "The :attribute must be a valid JSON string."
                    ], [
                        'trd_chave' => "validation.max.array",
                        'pt' => "O :attribute não deve ter mais que :max itens.",
                        'en' => "The :attribute may not have more than :max items."
                    ], [
                        'trd_chave' => "validation.max.file",
                        'pt' => "O :attribute não deve ser maior que :max kilobytes.",
                        'en' => "The :attribute may not be greater than :max kilobytes."
                    ], [
                        'trd_chave' => "validation.max.numeric",
                        'pt' => "O :attribute não deve ser maior que :max.",
                        'en' => "The :attribute may not be greater than :max."
                    ], [
                        'trd_chave' => "validation.max.string",
                        'pt' => "O :attribute não deve ser maior que :max caracteres.",
                        'en' => "The :attribute may not be greater than :max characters."
                    ], [
                        'trd_chave' => "validation.mimes",
                        'pt' => "O :attribute deve ser um arquivo do tipo: :values.",
                        'en' => "The :attribute must be a file of type: :values."
                    ], [
                        'trd_chave' => "validation.mimetypes",
                        'pt' => "O :attribute deve ser um arquivo do tipo: :values.",
                        'en' => "The :attribute must be a file of type: :values."
                    ], [
                        'trd_chave' => "validation.min.array",
                        'pt' => "O :attribute must have pelo menos :min itens.",
                        'en' => "The :attribute must have at least :min items."
                    ], [
                        'trd_chave' => "validation.min.file",
                        'pt' => "O :attribute deve ser pelo menos :min kilobytes.",
                        'en' => "The :attribute must be at least :min kilobytes."
                    ], [
                        'trd_chave' => "validation.min.numeric",
                        'pt' => "O :attribute deve ser pelo menos :min.",
                        'en' => "The :attribute must be at least :min."
                    ], [
                        'trd_chave' => "validation.min.string",
                        'pt' => "O :attribute deve ser pelo menos :min caracteres.",
                        'en' => "The :attribute must be at least :min characters."
                    ], [
                        'trd_chave' => "validation.not_in",
                        'pt' => "O :attribute selecionado é inválido.",
                        'en' => "The selected :attribute is invalid."
                    ], [
                        'trd_chave' => "validation.not_regex",
                        'pt' => "O formato do :attribute é inválido.",
                        'en' => "The :attribute format is invalid."
                    ], [
                        'trd_chave' => "validation.numeric",
                        'pt' => "O :attribute deve ser a number.",
                        'en' => "The :attribute must be a number."
                    ], [
                        'trd_chave' => "validation.present",
                        'pt' => "O campo :attribute deve ser present.",
                        'en' => "The :attribute field must be present."
                    ], [
                        'trd_chave' => "validation.regex",
                        'pt' => "O formato do :attribute é inválido.",
                        'en' => "The :attribute format is invalid."
                    ], [
                        'trd_chave' => "validation.required",
                        'pt' => "O campo :attribute é obrigatório.",
                        'en' => "The :attribute field is required."
                    ], [
                        'trd_chave' => "validation.required_if",
                        'pt' => "O campo :attribute é obrigatório quando :other é :value.",
                        'en' => "The :attribute field is required when :other is :value."
                    ], [
                        'trd_chave' => "validation.required_unless",
                        'pt' => "O campo :attribute é obrigatório a não ser que :other é em :values.",
                        'en' => "The :attribute field is required unless :other is in :values."
                    ], [
                        'trd_chave' => "validation.required_with",
                        'pt' => "O campo :attribute é obrigatório quando :values está presente.",
                        'en' => "The :attribute field is required when :values is present."
                    ], [
                        'trd_chave' => "validation.required_with_all",
                        'pt' => "O campo :attribute é obrigatório quando :values está presente.",
                        'en' => "The :attribute field is required when :values is present."
                    ], [
                        'trd_chave' => "validation.required_without",
                        'pt' => "O campo :attribute é obrigatório quando :values não está presente.",
                        'en' => "The :attribute field is required when :values is not present."
                    ], [
                        'trd_chave' => "validation.required_without_all",
                        'pt' => "O campo :attribute é obrigatório quando nenhum dos :values estão presentes.",
                        'en' => "The :attribute field is required when none of :values are present."
                    ], [
                        'trd_chave' => "validation.same",
                        'pt' => "O :attribute e :other devem ser iguais.",
                        'en' => "The :attribute and :other must match."
                    ], [
                        'trd_chave' => "validation.size.array",
                        'pt' => "O :attribute deve conter :size itens.",
                        'en' => "The :attribute must contain :size items."
                    ], [
                        'trd_chave' => "validation.size.file",
                        'pt' => "O :attribute deve ser :size kilobytes.",
                        'en' => "The :attribute must be :size kilobytes."
                    ], [
                        'trd_chave' => "validation.size.numeric",
                        'pt' => "O :attribute deve ser :size.",
                        'en' => "The :attribute must be :size."
                    ], [
                        'trd_chave' => "validation.size.string",
                        'pt' => "O :attribute deve ser :size caracteres.",
                        'en' => "The :attribute must be :size characters."
                    ], [
                        'trd_chave' => "validation.string",
                        'pt' => "O :attribute deve ser um texto.",
                        'en' => "The :attribute must be a string."
                    ], [
                        'trd_chave' => "validation.timezone",
                        'pt' => "O :attribute deve ser um fuso horário válido.",
                        'en' => "The :attribute must be a valid zone."
                    ], [
                        'trd_chave' => "validation.unique",
                        'pt' => "O :attribute já está em uso.",
                        'en' => "The :attribute has already been taken."
                    ], [
                        'trd_chave' => "validation.uploaded",
                        'pt' => "O :attribute falhou o upload.",
                        'en' => "The :attribute failed to upload."
                    ], [
                        'trd_chave' => "validation.url",
                        'pt' => "O formato do :attribute é inválido.",
                        'en' => "The :attribute format is invalid."
                    ],[
                        'trd_chave' => "app.db.perfil.administrador",
                        'pt' => "Administrador",
                        'en' => "Administrator"
                    ],[
                        'trd_chave' => "app.db.perfil.comum",
                        'pt' => "Comum",
                        'en' => "Common"
                    ],[
                        'trd_chave' => "app.screen.register.add",
                        'pt' => "Cadastro realizado com sucesso.",
                        'en' => NULL
                    ],[
                        'trd_chave' => "app.screen.register.remove",
                        'pt' => "Cadastro removido com sucesso.",
                        'en' => NULL
                    ],[
                        'trd_chave' => "app.question.msgRemove",
                        'pt' => "Deseja realmente excluir ?",
                        'en' => NULL
                    ],[
                        'trd_chave' => "app.standards.alertTitle.success",
                        'pt' => "SUCESSO !",
                        'en' => NULL
                    ],[
                        'trd_chave' => "app.standards.alertTitle.error",
                        'pt' => "ERRO !",
                        'en' => NULL
                    ],[
                        'trd_chave' => "app.standards.warning",
                        'pt' => "ATENÇÃO !",
                        'en' => NULL
                    ],[
                        'trd_chave' => "app.standards.mensages.defaultError",
                        'pt' => "Ocorreu um erro. Por favor contate o administrador.",
                        'en' => NULL
                    ],[
                        'trd_chave' => "app.meses.curto.jan",
                        'pt' => "Jan",
                        'en' => NULL
                    ], [
                        'trd_chave' => "app.meses.curto.fev",
                        'pt' => "Fev",
                        'en' => NULL
                    ], [
                        'trd_chave' => "app.meses.curto.mar",
                        'pt' => "Mar",
                        'en' => NULL
                    ], [
                        'trd_chave' => "app.meses.curto.abr",
                        'pt' => "Abr",
                        'en' => NULL
                    ], [
                        'trd_chave' => "app.meses.curto.mai",
                        'pt' => "Mai",
                        'en' => NULL
                    ], [
                        'trd_chave' => "app.meses.curto.jun",
                        'pt' => "Jun",
                        'en' => NULL
                    ], [
                        'trd_chave' => "app.meses.curto.jul",
                        'pt' => "Jul",
                        'en' => NULL
                    ], [
                        'trd_chave' => "app.meses.curto.ago",
                        'pt' => "Ago",
                        'en' => NULL
                    ], [
                        'trd_chave' => "app.meses.curto.set",
                        'pt' => "Set",
                        'en' => NULL
                    ], [
                        'trd_chave' => "app.meses.curto.out",
                        'pt' => "Out",
                        'en' => NULL
                    ], [
                        'trd_chave' => "app.meses.curto.nov",
                        'pt' => "Nov",
                        'en' => NULL
                    ], [
                        'trd_chave' => "app.meses.curto.dez",
                        'pt' => "Dez",
                        'en' => NULL
                    ], [
                        'trd_chave' => "app.meses.longo.jan",
                        'pt' => "Janeiro",
                        'en' => NULL
                    ], [
                        'trd_chave' => "app.meses.longo.fev",
                        'pt' => "Fevereiro",
                        'en' => NULL
                    ], [
                        'trd_chave' => "app.meses.longo.mar",
                        'pt' => "Março",
                        'en' => NULL
                    ], [
                        'trd_chave' => "app.meses.longo.abr",
                        'pt' => "Abril",
                        'en' => NULL
                    ], [
                        'trd_chave' => "app.meses.longo.mai",
                        'pt' => "Maio",
                        'en' => NULL
                    ], [
                        'trd_chave' => "app.meses.longo.jun",
                        'pt' => "Junho",
                        'en' => NULL
                    ], [
                        'trd_chave' => "app.meses.longo.jul",
                        'pt' => "Julho",
                        'en' => NULL
                    ], [
                        'trd_chave' => "app.meses.longo.ago",
                        'pt' => "Agosto",
                        'en' => NULL
                    ], [
                        'trd_chave' => "app.meses.longo.set",
                        'pt' => "Setembro",
                        'en' => NULL
                    ], [
                        'trd_chave' => "app.meses.longo.out",
                        'pt' => "Outubro",
                        'en' => NULL
                    ], [
                        'trd_chave' => "app.meses.longo.nov",
                        'pt' => "Novembro",
                        'en' => NULL
                    ], [
                        'trd_chave' => "app.meses.longo.dez",
                        'pt' => "Dezembro",
                        'en' => NULL
                    ],[
                        'trd_chave' => "app.meses.dias_semana.curto.dom",
                        'pt' => "Dom",
                        'en' => NULL
                    ], [
                        'trd_chave' => "app.meses.dias_semana.curto.seg",
                        'pt' => "Seg",
                        'en' => NULL
                    ], [
                        'trd_chave' => "app.meses.dias_semana.curto.ter",
                        'pt' => "Ter",
                        'en' => NULL
                    ], [
                        'trd_chave' => "app.meses.dias_semana.curto.qua",
                        'pt' => "Qua",
                        'en' => NULL
                    ], [
                        'trd_chave' => "app.meses.dias_semana.curto.qui",
                        'pt' => "Qui",
                        'en' => NULL
                    ], [
                        'trd_chave' => "app.meses.dias_semana.curto.sex",
                        'pt' => "Sex",
                        'en' => NULL
                    ], [
                        'trd_chave' => "app.meses.dias_semana.curto.sab",
                        'pt' => "Sáb",
                        'en' => NULL
                    ], [
                        'trd_chave' => "app.meses.dias_semana.longo.dom",
                        'pt' => "Domingo",
                        'en' => NULL
                    ], [
                        'trd_chave' => "app.meses.dias_semana.longo.seg",
                        'pt' => "Segunda-Feira",
                        'en' => NULL
                    ], [
                        'trd_chave' => "app.meses.dias_semana.longo.ter",
                        'pt' => "Terça-Feira",
                        'en' => NULL
                    ], [
                        'trd_chave' => "app.meses.dias_semana.longo.qua",
                        'pt' => "Quarta-Feira",
                        'en' => NULL
                    ], [
                        'trd_chave' => "app.meses.dias_semana.longo.qui",
                        'pt' => "Quinta-Feira",
                        'en' => NULL
                    ], [
                        'trd_chave' => "app.meses.dias_semana.longo.sex",
                        'pt' => "Sexta-Feira",
                        'en' => NULL
                    ], [
                        'trd_chave' => "app.meses.dias_semana.longo.sab",
                        'pt' => "Sábado",
                        'en' => NULL
                    ], [
                        'trd_chave' => "app.meses.dias_semana.min.dom",
                        'pt' => "D",
                        'en' => NULL
                    ], [
                        'trd_chave' => "app.meses.dias_semana.min.seg",
                        'pt' => "S",
                        'en' => NULL
                    ], [
                        'trd_chave' => "app.meses.dias_semana.min.ter",
                        'pt' => "T",
                        'en' => NULL
                    ], [
                        'trd_chave' => "app.meses.dias_semana.min.qua",
                        'pt' => "Q",
                        'en' => NULL
                    ], [
                        'trd_chave' => "app.meses.dias_semana.min.qui",
                        'pt' => "Q",
                        'en' => NULL
                    ], [
                        'trd_chave' => "app.meses.dias_semana.min.sex",
                        'pt' => "S",
                        'en' => NULL
                    ], [
                        'trd_chave' => "app.meses.dias_semana.min.sab",
                        'pt' => "S",
                        'en' => NULL
                    ], [
                        'trd_chave' => "app.datePicker.today",
                        'pt' => "Hoje",
                        'en' => NULL
                    ], [
                        'trd_chave' => "app.datePicker.clear",
                        'pt' => "Limpar",
                        'en' => NULL
                    ], [
                        'trd_chave' => "app.datePicker.format",
                        'pt' => "dd/mm/yyyy",
                        'en' => NULL
                    ], [
                        'trd_chave' => "app.datePicker.titleFormat",
                        'pt' => "MM yyyy",
                        'en' => NULL
                    ]

                ]);

                printf("\nTudo pronto, Have a little fun!!!");
            });
        }
        catch (Exception $e){

            printf("\nErro ! Rollback realizado");
        }
    }
}
