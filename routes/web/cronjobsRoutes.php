<?php
Route::name('cronjobs.')->prefix('cronjobs')->group(function()
{
	Route::any('/sincronizarestrutura','CronjobController@sincronizarestrutura')->name('sincronizarestrutura');
	Route::any('/sincronizarpackaging','CronjobController@sincronizarpackaging')->name('sincronizarpackaging');
	Route::any('/sincronizar',         'CronjobController@sincronizar')->name('sincronizar');
	Route::any('/sincronizarmalha',    'CronjobController@sincronizarmalha')->name('sincronizarmalha');
	Route::any('/traduzir',            'CronjobController@traduzir')->name('traduzir');
	Route::name('consolidar.')->prefix('consolidar')->group(function()
	{
		Route::any('/consumoeecompressores','CronjobController@consumoeecompressores')->name('consumoeecompressores');
		Route::any('/parametros',           'CronjobController@parametros')->name('parametros');
		Route::any('/parametrostmp',        'CronjobController@parametrostmp')->name('parametrostmp');
		Route::any('/historian',            'CronjobController@historian')->name('historian');
		Route::any('/utilidades',           'CronjobController@utilidades')->name('utilidades');
		Route::any('/logutilidades',        'CronjobController@logutilidades')->name('logutilidades');
	});
});