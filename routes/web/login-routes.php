<?php
Route::name('login.')->prefix('painel')->group(function(){
    Route::get('/login','LoginController@index')->name('index');
    Route::post('/login','LoginController@login')->name('login');
    Route::get('/logout','LoginController@logout')->name('logout');
});
