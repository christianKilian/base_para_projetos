<?php
Route::name('errors.')->prefix('erros')->group(function()
{
    Route::get('/acesso-negado','RedirectController@acessonegado')->name('acessonegado');
	Route::get('404','RedirectController@pageNotFound')->name('404');
	Route::get('403','RedirectController@generalError')->name('403');
	Route::get('500','RedirectController@acessonegado')->name('500');
});