<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

$glob_file = __DIR__.DIRECTORY_SEPARATOR.'api'.DIRECTORY_SEPARATOR.'*';
foreach(glob($glob_file) as $file)
{
	include $file;
}

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
