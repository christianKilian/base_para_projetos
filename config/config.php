<?php

return [
    'enable_query_log' => false,
    'cliente' => 'NOME PROJETO',
    'links' => [
        'webmail' => '',
    ],
    'emails' => [
        'subject_prefix' => '[VASP] - ',
        'from' => 'contato@vasp.com.br',
        'bcc'  => [
            'christian@chktecnologia.com.br',
            'rute@chktecnologia.com.br'
        ]
    ],
    'recaptcha' => [
        'key' => '',
        'secret' => ''
    ],
    'google' => [
        'analytics' => ''
    ],
    'paginator' => [
        'limit' => 10,
    ]
];