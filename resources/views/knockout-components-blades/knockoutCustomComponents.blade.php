@foreach(glob(base_path('resources'.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'knockout-components-blades'.DIRECTORY_SEPARATOR.'container').DIRECTORY_SEPARATOR.'*.blade.php') as $f)
	<?php
		$arquivo = str_replace(DIRECTORY_SEPARATOR, '.', 
			str_replace('.blade.php', '', 
				str_replace(base_path('resources'.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR), '', $f)
			)
		);
	?>
	@include($arquivo)
@endforeach