<template id="paginatortemplateko">
	<style type="text/css">
		.paginatorItem {
		    position: relative;
		    float: left;
		    padding: 6px 12px;
		    margin-left: -1px;
		    line-height: 1.42857143;
		    color: #337ab7;
		    text-decoration: none;
		    background-color: #fff;
		    border: 1px solid #ddd;
		    cursor:pointer;
		}

		.paginatorItem:hover,
		.paginatorItem:focus {
		    z-index: 3;
		    color: #23527c;
		    background-color: #eee;
		    border-color: #ddd;
		}

		.paginatorItem.active{
		    z-index: 2;
		    color: #fff;
		    background-color: #337ab7;
		    border-color: #337ab7;
		}
	</style>
	<div class="row mg-t-20">
		<div class="col-md-12">
			<div class="pagination float-left">
				<p data-bind="text: msgexibicao" style="font-size: 16px;"></p>
			</div>
			<div class="pagination float-right">
				<div class="paginatorItem" style="display:none;" data-bind=" click:navegar.bind($data, -1), visible:exibir_setas"> < </div>
				<div class="paginatorItem" style="display:nome;" data-bind="visible:bkdots"> ... </div>
				<!-- ko foreach:smartbuttons -->
					<div class="paginatorItem" data-bind=" click:$parent.click,text:pagina, css:{active:active}"></div>
				<!-- /ko -->
				<div class="paginatorItem" style="display:nome;" data-bind="visible:fwdots"> ... </div>
				<div class="paginatorItem" style="display:none;" data-bind="click:navegar.bind($data, 1),visible:exibir_setas"> > </div>
			</div>
		</div>
	</div>
</template>
<script type="text/javascript">
	<?php 
	/**
	* PAGINATOR COMPONENT 
		ATENÇÂO!!! tem uma função global no backend chamada _knockoutPaginator para padronizar o objeto igual o formato esperado por este componente
		esse comentário está dentro da tag php para não ser renderizada no browser... favor manter assim...
	* acepted params:
	*	total_de_paginas: observable com numero em integer da quantidade de paginas
	*	itens_por_pagina: observable com numero em integer da quantidade de itens por pagina
	*	total_de_itens: observable com numero em integer do total de registros
	*	pagina_atual: observable com numero em integer da pagina atual, usado para ativara cor do botão selecionado do componente
	*	callback: uma função que será chamada sempre que um click ou navegação for executada onde passará o numero da pagina relativa
	*  ex:
	*<paginator params="{
	*	total_de_paginas: ko.observable(10),
	*	itens_por_pagina: ko.observable(10),
	*	total_de_itens: ko.observable(108),
	*	pagina_atual: ko.observable(2),
	*	callback: function(pagina){base.post(url_filtro,{pagina:pagina},function(resp){})},		
	*}"></paginator>
	* observação é que os observables devem ser do seu contexto(viewModel) e não criados dinamicamente como no exemplo acima
	*/?>
	ko.components.register('paginator', {
		viewModel: function (params)
		{
			if( !ko.isObservable(params.total_de_paginas) ||
				!ko.isObservable(params.itens_por_pagina) ||
				!ko.isObservable(params.total_de_itens) ||
				!ko.isObservable(params.pagina_atual)){
				throw Error('Os parametros de entrada do paginator devem ser observables');
			}
			if(typeof(params.callback) != 'function') throw Error('O parametro callback é obrigatorio ser função');
			/**
			* @class Button
			* @description usado para tipar a lista de itens de navegação
			*/
			var Button = function(pagina)
			{
				var b = this;
				b.pagina = pagina;
				b.active = ko.computed(function(){
					return params.pagina_atual() == b.pagina;
				});
			};
			var self = this;
			self.template    = 'paginatortemplateko';//utilizado para aplicar o template no componente
			self.maxbtns     = 6; //esse parametro serve para limitar a lista de exibição dos botões
			self.bkdots      = ko.observable(false); // serve para exibir ou não os ... para anterior
			self.fwdots      = ko.observable(false);// serve para exibir ou não os ... para posterior
			self.msgexibicao = ko.computed(function()
			{
				var start,end
					total_de_paginas = parseFloat(ko.unwrap(params.total_de_paginas)),
					itens_por_pagina = parseFloat(ko.unwrap(params.itens_por_pagina)),
					total_de_itens   = parseFloat(ko.unwrap(params.total_de_itens)),
					pagina_atual     = parseFloat(ko.unwrap(params.pagina_atual))
				;
				if(total_de_paginas == 0) return '0 Registros';
				end = pagina_atual * itens_por_pagina;
				start = end - itens_por_pagina + 1;
				if(end > total_de_itens) end = total_de_itens; 
				return "Mostrando de "+start+" até "+end+" de "+total_de_itens+" registros";
			});
			self.click = function(btn)
			{
				if(btn.pagina == params.pagina_atual()) return;
				params.callback(btn.pagina);// como o callback vai chamar o backend, a vm que chamou o paginator atualizara o observable de pagina atual
			};
			self.navegar = function(direcao)
			{
				var dir = parseFloat(params.pagina_atual()) + (parseFloat(direcao));
				if(dir < 1 || dir > params.total_de_paginas()) return;
				params.callback(dir); // como o callback vai chamar o backend, a vm que chamou o paginator atualizara o observable de pagina atual
			};
			self.exibir_setas = ko.computed(function()
			{
				return params.total_de_paginas() > 1;
			});
			self.makebtns = function(start,end)
			{
				var tmp = [];
				if(start>end) return tmp;
				for(var i = start; i <= end; i++){
					tmp.push(new Button(i));
				}
				return tmp;
			}
			self.smartbuttons = ko.computed(function ()
			{
				var smartlist = [];
				var total = parseFloat(ko.unwrap(params.total_de_paginas));
				var atual = parseFloat(ko.unwrap(params.pagina_atual));
				self.bkdots(false);
				self.fwdots(false);
				if(total > 0)
				{
					if (total > self.maxbtns)
					{
						var start = atual -3,end = start+self.maxbtns;
						self.fwdots(true);
						if (atual >= self.maxbtns)
						{
							self.bkdots(true);
							if(end < total)
							{/* aqui faz a lista do tipo < ... 2 3 4 5 6 7 8 ... > */
								smartlist = self.makebtns(start,end);
							}
							else
							{/* aqui faz a lista do tipo < ... 2 3 4 5 6 7 8 > */
								end = total;
								start = end - self.maxbtns;
								self.fwdots(false);
								smartlist = self.makebtns(start,end);
							}
						}
						else
						{/* e aqui a lista do tipo < 1 2 3 4 5 6 7 ... > */
							smartlist = self.makebtns(1,self.maxbtns);
						}
					}
					else
					{/* e aqui uma lista simples sem os pontos < 1 2 3 4 5 6 7 > */
						smartlist = self.makebtns(1,total);
					}
				}
				return smartlist;
			}).extend({throttle:15});
		},
		template: '<!-- ko template:template --><!-- /ko -->'
	});
</script>