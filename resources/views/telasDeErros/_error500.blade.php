@extends('shared.masterpage')

@section('title', "Acesso Negado")

@section('content')
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <center>
            @if(!empty($mensagem))
            {{ $mensagem }}
            @else
            <h1>@lang("app.std.msg.ocorreu_erro")</h1>
            @endif
        </center>
        <br>
        <center><button onclick="javascrip:history.go(-1)" class="btn btn-info round">@lang("app.std.lbl.voltar")</button></center>
    </div>
</div>

@stop
